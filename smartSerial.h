/*
 * smartSerial.h
 *
 * Created: 15/6/2015 8:13:35 PM
 *  Author: Colin Tan
 */ 


#ifndef SMARTSERIAL_H_
#define SMARTSERIAL_H_

#include <avr/io.h>
#include <avr/interrupt.h>

#ifndef F_CPU
#define F_CPU	16000000
#endif


// ubrr array values for different clocks. We list only common frequencies of 8MHz, 16 MHz and 20 MHz. For other
// values see Atmel data sheet for the ATmega 2560 or 328P

#if F_CPU==8000000

	#define UBRR_ARRAY {207, 103, 51, 34, 25, 16, 12, 8, 6, 3};
		
#elif F_CPU==16000000

	#define UBRR_ARRAY {416, 207, 103, 68, 51, 34, 25, 16, 12, 8}

#elif F_CPU==20000000

	#define UBRR_ARRAY {520, 259, 129, 86, 64, 42, 32, 21, 15, 10};
	
#endif

typedef enum
{
	SERIAL_BAUD_2400 = 0,
	SERIAL_BAUD_4800 = 1,
	SERIAL_BAUD_9600 = 2,
	SERIAL_BAUD_14400 = 3,	
	SERIAL_BAUD_19200 = 4,
	SERIAL_BAUD_28800 = 5,
	SERIAL_BAUD_38400 = 6,
	SERIAL_BAUD_57600 = 7,
	SERIAL_BAUD_76800 = 8,
	SERIAL_BAUD_115200 = 9
} baudrate_t;

typedef enum
{
	SERIAL_7_DATABITS = 7,
	SERIAL_8_DATABITS = 8
} datasize_t;

typedef enum
{
	SERIAL_1_STOPBITS = 1,
	SERIAL_2_STOPBITS = 2	
} stopbits_t;

typedef enum
{
	SERIAL_NO_PARITY,
	SERIAL_EVEN_PARITY,
	SERIAL_ODD_PARITY	
} parity_t;

typedef enum
{
	SERIAL_PORT0,
	SERIAL_PORT1,
	SERIAL_PORT2,
	SERIAL_PORT3	
} ports_t;

#define MAXBUFLEN	32
#define MAXRECVLEN	64

extern "C" void USART0_RX_vect(void) __attribute__ ((signal));
extern "C" void USART0_TX_vect(void) __attribute__ ((signal));

#if defined(__AVR_ATmega2560__) || defined(__AVR_ATmega1280__)
extern "C" void USART1_RX_vect(void) __attribute__ ((signal));
extern "C" void USART1_TX_vect(void) __attribute__ ((signal));
extern "C" void USART2_RX_vect(void) __attribute__ ((signal));
extern "C" void USART2_TX_vect(void) __attribute__ ((signal));
extern "C" void USART3_RX_vect(void) __attribute__ ((signal));
extern "C" void USART3_TX_vect(void) __attribute__ ((signal));
#endif

class CSmartSerial
{
	protected:
		char _buffer[MAXBUFLEN];
		uint8_t _head, _tail, _len;	
		uint8_t _portNum;
		
		char _rcvBuffer[MAXRECVLEN];
		uint8_t _rhead, _rtail, _rlen;
		
	int enq(char ch, char buf[], uint8_t &head, uint8_t &len, uint8_t maxLen);
	char deq(char buf[], uint8_t &tail, uint8_t &len, uint8_t maxLen);
	
	void enableReceiver(uint8_t port);
	
	void initPort(ports_t portNum, baudrate_t baudRate, datasize_t dataBits, parity_t parity, stopbits_t stopBits);

	// Callback in event of character received
	virtual void processCharacterReceived(char ch);
			
	// Friend functions
	friend void USART0_RX_vect(void);
	friend void USART0_TX_vect(void);
	#if defined(__AVR_ATmega2560__) || defined(__AVR_ATmega1280__)
 	friend void USART1_RX_vect(void);
	friend void USART1_TX_vect(void);
	friend void USART2_RX_vect(void);
	friend void USART2_TX_vect(void);
	friend void USART3_RX_vect(void);
	friend void USART3_TX_vect(void);
	#endif
	
	
	public:
	
	CSmartSerial(ports_t portNum, baudrate_t baudRate, datasize_t dataBits, parity_t parity, stopbits_t stopBits);
	virtual ~CSmartSerial();
	
	void sendCh(char ch);
	void sendString(char *str);	
	
	// Check how many characters available for read is available
	uint8_t charAvail();
		
	// Get next character from buffer
	char getChar();
};

#endif /* SMARTSERIAL_H_ */