/*
 * smartSerial.cpp
 *
 * Created: 15/6/2015 8:12:28 PM
 *  Author: Colin Tan
 */ 


#include <avr/io.h>
#ifndef F_CPU
#define F_CPU 16000000
#endif
#include <util/delay.h>
#include "smartSerial.h"

CSmartSerial *_p;

ISR(USART0_RX_vect)
{
	char ch=UDR0;
	_p->processCharacterReceived(ch);
}

ISR(USART0_TX_vect)
{
	// If there's characters to send, send. Else disable the transmitter
	if(_p->_len)
		UDR0=_p->deq();
	else
		// Disable the interrupt
		UCSR0B &= ~(1<<TXCIE0);	
}
#if defined(__AVR_ATmega2560__)

ISR(USART1_RX_vect)
{
	char ch=UDR1;
	_p->processCharacterReceived(ch);
	
}

ISR(USART1_TX_vect)
{
	// If there's characters to send, send. Else disable the transmitter
	if(_p->_len)
		UDR1=_p->deq();
	else
		UCSR1B &= ~(1<<TXCIE1);
	
}

ISR(USART2_RX_vect)
{
		char ch=UDR2;
		_p->processCharacterReceived(ch);
}

ISR(USART2_TX_vect)
{
	// If there's characters to send, send. Else disable the transmitter
	if(_p->_len)
		UDR2=_p->deq();
	else
		UCSR2B &= ~(1<<TXCIE2);
	
}
ISR(USART3_RX_vect)
{
		char ch=UDR3;
		_p->processCharacterReceived(ch);

}

ISR(USART3_TX_vect)
{
		// If there's characters to send, send. Else disable the transmitter
	if(_p->_len)
		UDR3=_p->deq();
	else
		UCSR3B &= ~(1<<TXCIE3);

}

#endif

int CSmartSerial::enq(char ch)
{
	if(_len>=MAXBUFLEN)
		return -1;
	
	_buffer[_head]=ch;
	_head=(_head+1) % MAXBUFLEN;
	_len++;
	
	return _len;
}

char CSmartSerial::deq()
{
	if(_len==0)
		return 0;
	
	char ret=_buffer[_tail];
	_tail=(_tail+1) % MAXBUFLEN;
	_len--;
	
	return ret;
}

void CSmartSerial::processCharacterReceived(char ch)
{
	// Echo back
	sendCh(ch);

}

void CSmartSerial::initPort(ports_t portNum, baudrate_t baudRate, datasize_t dataBits, parity_t parity, stopbits_t stopBits)
{
	switch(portNum)
	{
		case 0:
		
			// Set all bits to 0
			UCSR0A=0;
			UCSR0B=0;
			UCSR0C=0;

			// Enable RX interrupt, disable TX interrupt			
			UCSR0B |= (1 << RXCIE0);
			UCSR0B &= ~(1 << TXCIE0);
			//UCSR0B &= ~(1<< UDRE0);
			
			// Enable receiver and transmitter
			UCSR0B |= (1<<TXEN0);
			UCSR0B |= (1<<RXEN0);
			
			// Set this bit to 0 because we are not going for 9-bit UART
			UCSR0B &= ~(1<<UCSZ02);
			
			// Set UMSEL to 00 to choose UART
			UCSR0C &= ~((1 << UMSEL00) | (1<<UMSEL01));
			
			// Default is no parity
			if(parity==SERIAL_ODD_PARITY)
			{
				UCSR0C |= ((1 << UPM00) | (1<<UPM01));
			}	
			else
				if(parity==SERIAL_EVEN_PARITY)
				{
					UCSR0C |= (1<<UPM01);
					UCSR0C &= ~(1<<UPM00);
				}
				else
				{
					UCSR0C &= ~((1<<UPM00) | (1<<UPM01));
				}
			
			// Set stop bits
			if(stopBits==SERIAL_1_STOPBITS)
				UCSR0C &= ~(1 << USBS0);
			else
				UCSR0C |= (1 << USBS0);
				
			// Set character size. Default is 8 bits
			if(dataBits==SERIAL_7_DATABITS)
			{
				UCSR0C |= (1<<UCSZ01);
				UCSR0C &= ~(1<<UCSZ00);
			}
			else
				UCSR0C |= ((1 << UCSZ00) | (1<<UCSZ01));

			// Finally set the baud rate
			UBRR0 = (F_CPU / (16 * baudRate))-1;
			
		break;
	
	#if defined(__AVR_ATmega2560__)	
		case 1:
			// Set all bits to 0
			UCSR1A=0;
			UCSR1B=0;
			UCSR1C=0;

			// Enable RX interrupt, disable TX interrupt
			UCSR1B |= (1 << RXCIE1);
			UCSR1B &= ~(1 << TXCIE1);
			//UCSR0B &= ~(1<< UDRE0);
			
			// Enable receiver and transmitter
			UCSR1B |= (1<<TXEN1);
			UCSR1B |= (1<<RXEN1);
			
			// Set this bit to 0 because we are not going for 9-bit UART
			UCSR1B &= ~(1<<UCSZ12);
			
			// Set UMSEL to 00 to choose UART
			UCSR1C &= ~((1 << UMSEL10) | (1<<UMSEL11));
			
			// Default is no parity
			if(parity==SERIAL_ODD_PARITY)
			{
				UCSR1C |= ((1 << UPM10) | (1<<UPM11));
			}
			else
			if(parity==SERIAL_EVEN_PARITY)
			{
				UCSR1C |= (1<<UPM11);
				UCSR1C &= ~(1<<UPM10);
			}
			else
			{
				UCSR1C &= ~((1<<UPM10) | (1<<UPM11));
			}
			
			// Set stop bits
			if(stopBits==SERIAL_1_STOPBITS)
				UCSR1C &= ~(1 << USBS1);
			else
				UCSR1C |= (1 << USBS1);
			
			// Set character size. Default is 8 bits
			if(dataBits==SERIAL_7_DATABITS)
			{
				UCSR1C |= (1<<UCSZ11);
				UCSR1C &= ~(1<<UCSZ10);
			}
			else
				UCSR1C |= ((1 << UCSZ10) | (1<<UCSZ11));

			// Finally set the baud rate
			UBRR1 = (F_CPU / (16 * baudRate))-1;
			

		break;
		
		case 2:
			// Set all bits to 0
			UCSR2A=0;
			UCSR2B=0;
			UCSR2C=0;

			// Enable RX interrupt, disable TX interrupt
			UCSR2B |= (1 << RXCIE2);
			UCSR2B &= ~(1 << TXCIE2);
			//UCSR0B &= ~(1<< UDRE0);
			
			// Enable receiver and transmitter
			UCSR2B |= (1<<TXEN2);
			UCSR2B |= (1<<RXEN2);
			
			// Set this bit to 0 because we are not going for 9-bit UART
			UCSR2B &= ~(1<<UCSZ22);
			
			// Set UMSEL to 00 to choose UART
			UCSR2C &= ~((1 << UMSEL20) | (1<<UMSEL21));
			
			// Default is no parity
			if(parity==SERIAL_ODD_PARITY)
			{
				UCSR2C |= ((1 << UPM20) | (1<<UPM21));
			}
			else
			if(parity==SERIAL_EVEN_PARITY)
			{
				UCSR2C |= (1<<UPM21);
				UCSR2C &= ~(1<<UPM20);
			}
			else
			{
				UCSR2C &= ~((1<<UPM20) | (1<<UPM21));
			}
			
			// Set stop bits
			if(stopBits==SERIAL_1_STOPBITS)
				UCSR2C &= ~(1 << USBS2);
			else
				UCSR2C |= (1 << USBS2);
			
			// Set character size. Default is 8 bits
			if(dataBits==SERIAL_7_DATABITS)
			{
				UCSR2C |= (1<<UCSZ21);
				UCSR2C &= ~(1<<UCSZ20);
			}
			else
				UCSR2C |= ((1 << UCSZ20) | (1<<UCSZ21));

			// Finally set the baud rate
			UBRR2 = (F_CPU / (16 * baudRate))-1;
			
		break;
		
		case 3:
			// Set all bits to 0
			UCSR3A=0;
			UCSR3B=0;
			UCSR3C=0;

			// Enable RX interrupt, disable TX interrupt
			UCSR3B |= (1 << RXCIE3);
			UCSR3B &= ~(1 << TXCIE3);
			//UCSR0B &= ~(1<< UDRE0);
			
			// Enable receiver and transmitter
			UCSR3B |= (1<<TXEN3);
			UCSR3B |= (1<<RXEN3);
			
			// Set this bit to 0 because we are not going for 9-bit UART
			UCSR3B &= ~(1<<UCSZ32);
			
			// Set UMSEL to 00 to choose UART
			UCSR3C &= ~((1 << UMSEL30) | (1<<UMSEL31));
			
			// Default is no parity
			if(parity==SERIAL_ODD_PARITY)
			{
				UCSR3C |= ((1 << UPM30) | (1<<UPM31));
			}
			else
			if(parity==SERIAL_EVEN_PARITY)
			{
				UCSR3C |= (1<<UPM31);
				UCSR3C &= ~(1<<UPM30);
			}
			else
			{
				UCSR3C &= ~((1<<UPM30) | (1<<UPM31));
			}
			
			// Set stop bits
			if(stopBits==SERIAL_1_STOPBITS)
				UCSR3C &= ~(1 << USBS3);
			else
				UCSR3C |= (1 << USBS3);
			
			// Set character size. Default is 8 bits
			if(dataBits==SERIAL_7_DATABITS)
			{
				UCSR3C |= (1<<UCSZ31);
				UCSR3C &= ~(1<<UCSZ30);
			}
			else
				UCSR3C |= ((1 << UCSZ30) | (1<<UCSZ31));

			// Finally set the baud rate
			UBRR3 = (F_CPU / (16 * baudRate))-1;
			
					
		break;
#endif
		default:		
		break;
	}
	// Activate interrupts
	sei();	
}
	
CSmartSerial::CSmartSerial(ports_t portNum, baudrate_t baudRate, datasize_t dataBits, parity_t parity, stopbits_t stopBits)
{
	_p=this;
	_portUsed[portNum]=1;
	_portNum=portNum;
	initPort(portNum, baudRate, dataBits, parity, stopBits);
}

CSmartSerial::~CSmartSerial()
{
}
	
void CSmartSerial::sendCh(char ch)
{
	if(_len>0)
		enq(ch);
	else
		switch(_portNum)
		{
			case 0:
				UDR0=ch;
			break;
		
			#if defined(__AVR_ATmega2560__)
		
			case 1:
				UDR0=ch;
			break;
		
			case 2:
				UDR0=ch;
			break;
		
			case 3:
				UDR0=ch;
			break;
		
			#endif
		
			default:
			break;
		}
}

void CSmartSerial::sendString(char *str)
{
	uint8_t ctr=0;
	
	int sendFirstChar = (_len==0);

	while(char ch=str[ctr++])
	{
		enq(ch);
	}

	if(sendFirstChar)
		switch(_portNum)
		{
			case 0:
				UDR0=deq();
				UCSR0B |= (1<<TXCIE0);
				break;
		
			#if defined(__AVR_ATmega2560__)
		
			case 1:
				UDR1=deq();
				UCSR1B |= (1<<TXCIE1);
			break;
		
			case 2:
				UDR2=deq();
				UCSR2B |= (1<<TXCIE2);
			break;
		
			case 3:
				UDR3=deq();
				UCSR3B |= (1<<TXCIE3);
			break;
		
			#endif
		
			default:
			break;
		}
}

