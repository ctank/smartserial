/*
 * testSerial.cpp
 *
 * Created: 16/6/2015 7:39:51 AM
 *  Author: dcstanc
 */ 

#include "smartSerial.h"
#include <util/delay.h>
int main()
{
	CSmartSerial serial(SERIAL_PORT0, SERIAL_BAUD_9600, SERIAL_8_DATABITS, SERIAL_NO_PARITY, SERIAL_1_STOPBITS);
	// Just loop
	serial.sendString("Hello world!\n");
	while(1)
	{
	}
}