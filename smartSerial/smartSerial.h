/*
 * smartSerial.h
 *
 * Created: 15/6/2015 8:13:35 PM
 *  Author: Colin Tan
 */ 


#ifndef SMARTSERIAL_H_
#define SMARTSERIAL_H_

#include <avr/io.h>
#include <avr/interrupt.h>

#ifndef F_CPU
#define F_CPU	16000000
#endif

typedef enum
{
	SERIAL_BAUD_300 = 300,
	SERIAL_BAUD_1200 = 1200,
	SERIAL_BAUD_2400 = 2400,
	SERIAL_BAUD_4800 = 4800,
	SERIAL_BAUD_9600 = 9600,
	SERIAL_BAUD_14400 = 14400,	
	SERIAL_BAUD_19200 = 19200,
	SERIAL_BAUD_28800 = 28800,
	SERIAL_BAUD_38400 = 38400,
	SERIAL_BAUD_56000 = 56000,
	SERIAL_BAUD_57600 = 57600,
	SERIAL_BAUD_115200 = 115200
} baudrate_t;

typedef enum
{
	SERIAL_7_DATABITS = 7,
	SERIAL_8_DATABITS = 8
} datasize_t;

typedef enum
{
	SERIAL_1_STOPBITS = 1,
	SERIAL_2_STOPBITS = 2	
} stopbits_t;

typedef enum
{
	SERIAL_NO_PARITY,
	SERIAL_EVEN_PARITY,
	SERIAL_ODD_PARITY	
} parity_t;

typedef enum
{
	SERIAL_PORT0,
	SERIAL_PORT1,
	SERIAL_PORT2,
	SERIAL_PORT3	
} ports_t;

#define MAXBUFLEN	32

extern "C" void USART0_RX_vect(void) __attribute__ ((signal));
extern "C" void USART0_TX_vect(void) __attribute__ ((signal));

#if defined(__AVR_ATmega2560__) || defined(__AVR_ATmega1280__)
extern "C" void USART1_RX_vect(void) __attribute__ ((signal));
extern "C" void USART1_TX_vect(void) __attribute__ ((signal));
extern "C" void USART2_RX_vect(void) __attribute__ ((signal));
extern "C" void USART2_TX_vect(void) __attribute__ ((signal));
extern "C" void USART3_RX_vect(void) __attribute__ ((signal));
extern "C" void USART3_TX_vect(void) __attribute__ ((signal));
#endif

class CSmartSerial
{
	protected:
		char _buffer[MAXBUFLEN];
		uint8_t _head, _tail, _len;	
		uint8_t _portNum;
		uint8_t sregVal;
		
	int enq(char ch);
	char deq();
	
	void enableReceiver(uint8_t port);
	
	void initPort(ports_t portNum, baudrate_t baudRate, datasize_t dataBits, parity_t parity, stopbits_t stopBits);

	virtual void processCharacterReceived(char ch);
			
	// Friend functions
	friend void USART0_RX_vect(void);
	friend void USART0_TX_vect(void);
	#if defined(__AVR_ATmega2560__) || defined(__AVR_ATmega1280__)
 	friend void USART1_RX_vect(void);
	friend void USART1_TX_vect(void);
	friend void USART2_RX_vect(void);
	friend void USART2_TX_vect(void);
	friend void USART3_RX_vect(void);
	friend void USART3_TX_vect(void);
	uint8_t _portUsed[5];
	#else
	uint8_t _portUsed[1];
	#endif
	
	
	public:
	
	CSmartSerial(ports_t portNum, baudrate_t baudRate, datasize_t dataBits, parity_t parity, stopbits_t stopBits);
	~CSmartSerial();
	
	void sendCh(char ch);
	void sendString(char *str);	
};

#endif /* SMARTSERIAL_H_ */