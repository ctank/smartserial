/*
 * testSerial.cpp
 *
 * Created: 16/6/2015 7:39:51 AM
 *  Author: dcstanc
 */ 

#include "smartSerial.h"
#include <util/delay.h>
#include <stdio.h>
int main()
{
	CSmartSerial *serial = new CSmartSerial(SERIAL_PORT0, SERIAL_BAUD_9600, SERIAL_8_DATABITS, SERIAL_NO_PARITY, SERIAL_1_STOPBITS);
	CSmartSerial *serial2 = new CSmartSerial(SERIAL_PORT1, SERIAL_BAUD_9600, SERIAL_8_DATABITS, SERIAL_NO_PARITY, SERIAL_1_STOPBITS);
	char buffer[32];
	while(1)
	{
		sprintf(buffer, "Characters for read: %d\n", serial->charAvail());
		serial->sendString(buffer);
		serial2->sendString("Hello fish\n");
		_delay_ms(500);
	}
}